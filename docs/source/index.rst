.. Trustico API documentation master file, created by
   sphinx-quickstart on Wed Jun 13 14:23:10 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Trustico API documentation!
======================================================

.. toctree::
   :maxdepth: 2
   :caption: Getting Started

   Welcome/Welcome.rst

.. toctree::
   :maxdepth: 2
   :caption: Hello

   Hello/Hello.rst

.. toctree::
   :maxdepth: 2
   :caption: Managing Your Orders

   Order/parsecsr
   Order/getorderstatus
   Order/resendemail
   Order/reissue
   Order/getuseragreement
   Order/getapproverlist
   Order/placeorder
   Order/getbalance
   Order/getproductprice
   Order/markasrefunded
   Order/cancelorder
   Order/getcertificate
   Order/getca
   Order/getorderdetails
