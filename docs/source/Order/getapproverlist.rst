
-----------------------------------
*GetApproverList*
-----------------------------------


**POST** */order/getapproverlist*


	This method is used to obtain the complete allowed Approver E-Mail address list for a particular CSR Domain Name. This method is optional but can be used prior to process Domain Vetted orders. This method returns multi level Generic e-mails and usable WHOIS contact addresses (when applicable).



|
|	

	**Request Body** *application/json*
	
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^



	+-----------------------------------------------------+--------------+
	| Schema                                              |              |
	+-----------------------------------------------------+--------------+
	| array[object]                                       |              |
	+--------+------------+--------+----------------------+--------------+
	|        | DomainName | string | A valid domain name. | **Required** |
	+--------+------------+--------+----------------------+--------------+

|


|

	**Responses** *application/json*
	
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


	+-----+---------------------------------------------------------------------------------------------+--------------+
	| 200 | Schema                                                                                      |              |
	+-----+---------------------------------------------------------------------------------------------+--------------+
	|     | array[object]                                                                               |              |
	+-----+--+--------------+---------+-----------------------------------------------------------------+--------------+
	|     |  | SuccessCode  | integer | 1 (Ok) - 0 (Error)                                              | **Required** |
	+-----+--+--------------+---------+-----------------------------------------------------------------+--------------+
	|     |  | ApproverList | array   | It returns a list of Approver E-Mail addresses                  | *optional*   |
	+-----+--+--------------+---------+-----------------------------------------------------------------+--------------+
	|     |  | ErrorMsg     | array   | One or more error strings (Returned only when SuccessCode is 0) | *optional*   |
	+-----+--+--------------+---------+-----------------------------------------------------------------+--------------+

	
