
-----------------------------------
*GetOrderDetails*
-----------------------------------

**POST** */order/getorderdetails*


	This method is used to get the details of your order. This method will return product details, CSR details, SAN details (if applicable), order admin details, order technical details and order status for the given Order Reference.
	
|
|	

	**Request Body** *application/json*
	
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


	
	+--------------------------------------------------------------------------------------------------------------------+--------------+
	| Schema                                                                                                             |              |
	+--------------------------------------------------------------------------------------------------------------------+--------------+
	| array[object]                                                                                                      |              |
	+--+----------------+---------+--------------------------------------------------------------------------------------+--------------+
	|  | OrderReference | integer | A valid integer order reference provided by Trustico® when you have placed an order. | **Required** |
	+--+----------------+---------+--------------------------------------------------------------------------------------+--------------+
	
|

|


	**Responses** *application/json*
	
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	
	
	+-----+-------------------------------------------------------------------------------------------------------+--------------+
	| 200 | Schema                                                                                                |              |
	+-----+-------------------------------------------------------------------------------------------------------+--------------+
	|     | array[object]                                                                                         |              |
	+-----+--+-----------------------+---------+------------------------------------------------------------------+--------------+
	|     |  | SuccessCode           | integer | 1 (Ok) - 0 (Error)                                               | **Required** |
	+-----+--+-----------------------+---------+------------------------------------------------------------------+--------------+
	|     |  | Errormsg              | array   | One or more error strings (Returned only when SuccessCode is 0). | *optional*   |
	+-----+--+-----------------------+---------+------------------------------------------------------------------+--------------+
	|     |  | Certificate           | string  | It returns actual SSL certificate.                               | *optional*   |
	+-----+--+-----------------------+---------+------------------------------------------------------------------+--------------+
	|     |  | CertificateStartDate  | string  | It returns SSL certificate start date.                           | *optional*   |
	+-----+--+-----------------------+---------+------------------------------------------------------------------+--------------+
	|     |  | CertificateExpiryDate | string  | It returns SSL certificate expiry date.                          | *optional*   |
	+-----+--+-----------------------+---------+------------------------------------------------------------------+--------------+
	|     |  | CertificateStatus     | string  | It returns SSL certificate status.                               | *optional*   |
	+-----+--+-----------------------+---------+------------------------------------------------------------------+--------------+
	|     |  | IntermediateCAs       | string  | It returns intermediate CA certificates                          | *optional*   |
	+-----+--+-----------------------+---------+------------------------------------------------------------------+--------------+
		