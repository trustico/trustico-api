
-----------------------------------
*GetProductPrice*
-----------------------------------

**POST** */order/getproductprice*



	This method is used to return the default prices of any particular product. It will also return the SAN Pack, Individual SAN, Individual Wildcard SAN prices wherever its applicable.
	
|
|	

	**Request Body** *application/json*
	
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


		
	+------------------------------------------------------------------------------------------------------------------------+--------------+
	| Schema                                                                                                                 |              |
	+--+---------------------------------------------------------------------------------------------------------------------+--------------+
	| array[object]                                                                                                          |              |
	+--+----------------+---------+------------------------------------------------------------------------------------------+--------------+
	|  | ProductCode    | integer | A valid ProductCode                                                                      | **Required** |
	|  |                |         | See product table.                                                                       |              |
	+--+----------------+---------+------------------------------------------------------------------------------------------+--------------+
	|  | ValidityPeriod | integer | A valid product validity period in months, i.e. 12, 24                                   | **Required** |
	|  |                |         | See 'product list' for all names and validity periods                                    |              |
	+--+----------------+---------+------------------------------------------------------------------------------------------+--------------+
	|  | Attribute      | string  | N = New, R = Renew                                                                       | **Required** |
	+--+----------------+---------+------------------------------------------------------------------------------------------+--------------+
	|  | Country        | string  | A valid ISO-2 country code,                                                              | **Required** |
	|  |                |         | i.e JP (If you want to get Japanese price and its case-sensitive otherwise leave blank). |              |
	+--+----------------+---------+------------------------------------------------------------------------------------------+--------------+


|

|

	**Responses** *application/json*
	
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		
	+-----+------------------------------------------------------------------------------------------------------------+--------------+
	| 200 | Schema                                                                                                     |              |
	+-----+------------------------------------------------------------------------------------------------------------+--------------+
	|     | array[object]                                                                                              |              |
	+-----+--+----------------------------+---------+------------------------------------------------------------------+--------------+
	|     |  | SuccessCode                | integer | 1 (Ok) - 0 (Error)                                               | **Required** |
	+-----+--+----------------------------+---------+------------------------------------------------------------------+--------------+
	|     |  | ProductPrice               | string  | It returns product price with currency symbol.                   | *optional*   |
	+-----+--+----------------------------+---------+------------------------------------------------------------------+--------------+
	|     |  | ErrorMsg                   | array   | One or more error strings (Returned only when SuccessCode is 0). | *optional*   |
	+-----+--+----------------------------+---------+------------------------------------------------------------------+--------------+
	|     |  | SANPackPrice               | string  | It returns SAN pack price with currency symbol.                  | *optional*   |
	+-----+--+----------------------------+---------+------------------------------------------------------------------+--------------+
	|     |  | SANIndividualPrice         | string  | It returns SAN individual price with currency symbol.            | *optional*   |
	+-----+--+----------------------------+---------+------------------------------------------------------------------+--------------+
	|     |  | SANIndividualWildcardPrice | string  | It returns SAN individual wildcard price with currency symbol.   | *optional*   |
	+-----+--+----------------------------+---------+------------------------------------------------------------------+--------------+



