

-----------------------------------
*CancelOrder*
-----------------------------------

**POST** */order/cancelorder*


	This method is used to Cancel a “Processing” order and refund to reseller’s balance. This method can only be used when supplier order status is PENDING. Attempting to cancel an order at any other stage will fail.

	For GeoTrust and RapidSSL you can cancel the order within 30-days after placing order for suppliers.

	
|
|	

	**Request Body** *application/json*
	
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


		
	+----------------------------------------------------------------------+--------------+
	| Schema                                                               |              |
	+----------------------------------------------------------------------+--------------+
	| array[object]                                                        |              |
	+--+----------------+---------+----------------------------------------+--------------+
	|  | OrderReference | integer | A valid integer order reference number | **Required** |
	+--+----------------+---------+----------------------------------------+--------------+
	
|

|

	**Responses** *application/json*
	
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		
	+-----+------------------------------------------------------------------------------------------------+--------------+
	| 200 | Schema                                                                                         |              |
	+-----+------------------------------------------------------------------------------------------------+--------------+
	|     | array                                                                                          |              |
	+-----+--+----------------+---------+------------------------------------------------------------------+--------------+
	|     |  | SuccessCode    | integer | 1 (Ok) - 0 (Error)                                               | **Required** |
	+-----+--+----------------+---------+------------------------------------------------------------------+--------------+
	|     |  | RefundedAmount | string  | It returns total refunded amount with currency.                  | *optional*   |
	+-----+--+----------------+---------+------------------------------------------------------------------+--------------+
	|     |  | ErrorMsg       | array   | One or more error strings (Returned only when SuccessCode is 0). | *optional*   |
	+-----+--+----------------+---------+------------------------------------------------------------------+--------------+
	|     |  | OrderReference | integer | Order Reference.                                                 | *optional*   |
	+-----+--+----------------+---------+------------------------------------------------------------------+--------------+

	